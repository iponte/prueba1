/*********************************************************************/
/*Autor: Idelina Ponte                                               */
/*Objetivo del archivo: Definir  las funciones del controlador       */
/*********************************************************************/
var app = angular.module("app", []);
/*Funcion cuya finalidad es obtener los datos en json*/
function RemoteResource($http,$q, baseUrl) {
  this.get = function() {
    var defered=$q.defer();
    var promise=defered.promise;
    
    $http({
      method: 'GET',
      // se guardó en un archivo json por no poder conectarse a la página con la clave. 
      url: baseUrl + '/datos.json'
      //url: 'http://www.omdbapi.com/?i=tt3896198&apikey=86132bc1'
    }).success(function(data, status, headers, config) {
       defered.resolve(data);
    }).error(function(data, status, headers, config) {
       defered.reject(status);
    });
    
    return promise;
    
  }
  this.list = function() {
    var defered=$q.defer();
    var promise=defered.promise;    
    
    $http({
      method: 'GET',
      // se guardó en un archivo json por no poder conectarse a la página con la clave. 
      url: baseUrl + '/listado_peliculas.json'
      // url: baseUrl + 'http://www.omdbapi.com/?i=tt3896198&apikey=86132bc1'
    }).success(function(data, status, headers, config) {
      defered.resolve(data);
    }).error(function(data, status, headers, config) {
      defered.reject(status);
    });
    return promise;
  }
}
//Proveedor de recursos remotos 
function RemoteResourceProvider() {
  var _baseUrl;
  this.setBaseUrl = function(baseUrl) {
    _baseUrl = baseUrl;
  }
  this.$get = ['$http','$q',function($http,$q) {
      return new RemoteResource($http,$q, _baseUrl);
  }];
}

app.provider("remoteResource", RemoteResourceProvider);

app.constant("baseUrl", ".");
app.config(['baseUrl', 'remoteResourceProvider',
  function(baseUrl, remoteResourceProvider) {
    remoteResourceProvider.setBaseUrl(baseUrl);
  }
]);
// logo y ruta del logo
app.value("urlLogo", "logo.jpg");
app.run(["$rootScope", "urlLogo",function($rootScope, urlLogo) {
    $rootScope.urlLogo = urlLogo;
}]);

/*función para filtrar la búsqueda*/
app.filter("filtro",["$filter",function($filter) {
  var filterFn=$filter("filter");
  /** Transforma el texto quitando todos los acentos diéresis, etc. **/
  function normalize(texto) {
    texto = texto.replace(/[áàäâ]/g, "a");
    texto = texto.replace(/[éèëê]/g, "e");
    texto = texto.replace(/[íìïî]/g, "i");
    texto = texto.replace(/[óòôö]/g, "o");
    texto = texto.replace(/[úùüü]/g, "u");
    texto = texto.toUpperCase();
    return texto;
  }
    
  /** Esta función es el comparator en el filter **/
  function comparator(actual, expected) {
      if (normalize(actual).indexOf(normalize(expected))>=0) {
        return true;
      } else {
        return false;
      }
  }
   
  /** filtro para realizar la búsqueda**/
  function filtro(array,expression) {
    return filterFn(array,expression,comparator)
  }
  return filtro;
   
}]);

app.controller("DetallePeliculasController", ['$scope', 'remoteResource',function($scope, remoteResource) {
    $scope.filtro = {
      title: "",
      year: "",
      poster: "",
      country: "",
      imdbID: ""
    }

   remoteResource.get().then(function(pelicula) {
      $scope.pelicula = pelicula;
    }, function(status) {
      alert("Ha fallado la petición. Estado HTTP:" + status);
    });

}]);
/* tomar la información del listado y asignarlo al localStorage*/
app.controller("ListadoPeliculasController", ['$scope', 'remoteResource',  function($scope, remoteResource) {
    $scope.peliculas = [];
    $scope.peliculas2 = [];
    remoteResource.list().then(function(peliculas) {
      $scope.peliculas = peliculas;
    }, function(status) {
      alert("Ha fallado la petición. Estado HTTP:" + status);
    });

  /* localStorage, validación y muestra de los datos*/  
   $scope.GetValue = function () {
    var u=0;
    localStorage.clear();
    var hay=0;
    for ( var i = 0; i < $scope.peliculas.length; i++) {
      if ($scope.peliculas[i].selected) {
        hay=1;
        $scope.peliculas2[u]=$scope.peliculas[i];
        u=u+1;
      }
    }
    if (hay==0){
        swal({
          title: "Atencion",
          text: "Debe seleccionar sus peliculas favoritas",
          icon: "error",
        });
    }else{
     // Guardo el objeto como un string
         localStorage.setItem('datos', JSON.stringify($scope.peliculas2));
        // Obtengo el string previamente salvado y luego 
         var guardado = localStorage.getItem('datos');
         // imprimir detallado la información
         var message = "" ;
         var o=1;
         for ( var i = 0; i < $scope.peliculas.length; i++) {
           if ($scope.peliculas[i].selected) {
              var peliculaId = $scope.peliculas[i].imdbID;
              var peliculaName = $scope.peliculas[i].Title;
              message += o+".- "+ peliculaId + " - " + peliculaName + "\n" ;
              o=o+1;
           }
         }
         var texto=message+ "\n"+" Json: \n "+guardado;
         swal({
              title: "Peliculas Favoritas en el localStorage:",
              text: texto,
              icon: "info",
              button: "Continuar",
            });
    }
  }

   $scope.Cancelar = function () {
    localStorage.clear();
    sessionStorage.clear();
    window.location="index.html";
   }
}]);