/***********************************************************************/
/*Autor: Idelina Ponte                                                 */
/*Objetivo del archivo: Asignar las variables a la sessión del Storage */
/***********************************************************************/
$(document).ready(function(){
  $('#txtUsuario').focus();
});

// Generar el evento Submit del formulario
$("#formulario_ingreso").submit(function(e){
 e.preventDefault();
  //Asignamos el valor al objeto SessionStorage
 sessionStorage.setItem('usuario',$('#txtUsuario').val());
  //Asignamos el valor al objeto localStorage
 localStorage.setItem('password',$('#txtPassword').val());
 window.location = 'localStorage.html';
});