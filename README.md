Autor Idelina Ponte
La finalidad de esta aplicación es:
1.- Obtención de datos de un API REST en http://www.omdbapi.com/ (key a utilizar: f12ba140). Esta clave no me permitía la descarga de la información, razón por la cual tome alguna data de la página y la coloqué en un archivo json. Luego utilicé la url: 'http://www.omdbapi.com/?i=tt3896198&apikey=86132bc1' que me fue asignada cuando solicité la key, pero obtenía únicamente un registro por el id del registro.
2.- Visualización de películas, las peticiones hay que hacerlas con promesas.
3.- Visualización de detalle de cada película .
4.- Añadir películas favoritas en el localstorage.
5.- Se empleó AngularJS, Boostrap y SweetAlert.
6.- La ruta utilizada en mi local fue localhost/www/gfi_idelina_def/index.html